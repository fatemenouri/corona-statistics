const spinner = document.getElementById("spinner");

function getData(url = "") {
    showSpinner();
    const response = fetch(url, {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
        })
        .then((response) => response.json())
        .then((data) => {
            createHtml(data);
        });
}

function countryCheck() {

    var country = document.getElementById("country").value;
    if (country == "") {
        alert("country must be filled out");
        return false;
    } else if (country.length < 3) {
        alert("country must have 3 or more words");
        return false;
    }

    getData(`https://corona.lmao.ninja/v2/countries/${country}`);
}

function createHtml(data) {
    document.getElementById("flag").src = data.countryInfo.flag;
    document.getElementById("country").innerHTML = data.country;
    document.getElementById("active").innerHTML = data.active.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    document.getElementById("critical").innerHTML = data.critical.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    document.getElementById("recovered").innerHTML = data.active.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    document.getElementById("total").innerHTML = data.cases.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    document.getElementById("deaths").innerHTML = data.deaths.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    document.getElementById("tests").innerHTML = data.tests.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function showSpinner() {
    spinner.classList.add("show");
    setTimeout(() => {
        spinner.classList.remove("show");
    }, 500);
}